### Hi there 👋

Say Hello, I don't bite!

#### 📫 How to reach me

- Fediverse 🐘: https://mamot.fr/@arthurlutzim
- Homepage 🏠: https://arthur.lutz.im/
- Blog 📰: https://arthur.lutz.im/blog/
- Twitter 🐦: https://twitter.com/arthurlutzim

#### 👷 Check out what I'm currently working on

- [bramstroker/homeassistant-powercalc](https://github.com/bramstroker/homeassistant-powercalc) - Custom component to calculate estimated power consumption of lights and other appliances (1 week ago)
- [AmberELEC/website](https://github.com/AmberELEC/website) - AmberELEC website (2 months ago)
- [InfiniTimeOrg/InfiniSim](https://github.com/InfiniTimeOrg/InfiniSim) - Simulator for InfiniTime user interface without needing a PineTime (9 months ago)
- [scanlime/mixcandy](https://github.com/scanlime/mixcandy) - Music Hack Day SF, 2014 (10 months ago)
- [Menturan/MMM-OpeningHours](https://github.com/Menturan/MMM-OpeningHours) - Magic Mirror module that displays places opening hours. (11 months ago)
- [arthurlutz/dashlord](https://github.com/arthurlutz/dashlord) -  (11 months ago)
- [crowdsecurity/cs-firewall-bouncer](https://github.com/crowdsecurity/cs-firewall-bouncer) - Crowdsec bouncer written in golang for firewalls (1 year ago)

#### 🌱 My latest projects

- [arthurlutz/dashlord](https://github.com/arthurlutz/dashlord) - 
- [arthurlutz/adventure-maps](https://github.com/arthurlutz/adventure-maps) - 
- [arthurlutz/awesome-software-running-on-old-hardware](https://github.com/arthurlutz/awesome-software-running-on-old-hardware) - Abandonware on old hardware renders it unsecure, partially unusuable. Let&#39;s upgrade or replace it. (mirror)
- [arthurlutz/PersonalDataDotIO.github.io](https://github.com/arthurlutz/PersonalDataDotIO.github.io) - 

#### 🔭 Latest releases I've contributed to

- [bramstroker/homeassistant-powercalc](https://github.com/bramstroker/homeassistant-powercalc) ([v1.1.6](https://github.com/bramstroker/homeassistant-powercalc/releases/tag/v1.1.6), 1 week ago) - Custom component to calculate estimated power consumption of lights and other appliances
- [crowdsecurity/cs-firewall-bouncer](https://github.com/crowdsecurity/cs-firewall-bouncer) ([v0.0.25-rc2](https://github.com/crowdsecurity/cs-firewall-bouncer/releases/tag/v0.0.25-rc2), 2 months ago) - Crowdsec bouncer written in golang for firewalls

#### 📜 My recent blog posts

- [Appareil Android en mode hors-ligne – pratiques](https://arthur.lutz.im/blog/2022/10/17/appareil-android-en-mode-hors-ligne-pratiques/) (1 month ago)
- [Explorations clavier midi Launchpad novation](https://arthur.lutz.im/blog/2022/02/28/explorations-clavier-midi-launchpad-novation/) (9 months ago)
- [PinCab à partir d’un Gottlieb 1968 – tribulations](https://arthur.lutz.im/blog/2022/02/27/pincab-a-partir-dun-gottlieb-1968-tribulations/) (9 months ago)
- [Stage construction d’un panneau chauffage solaire low-tech](https://arthur.lutz.im/blog/2022/02/27/stage-construction-dun-panneau-chauffage-solaire-low-tech/) (9 months ago)
- [Travail du bois – chantier participatif et atelier](https://arthur.lutz.im/blog/2022/02/24/travail-du-bois-chantier-participatif-et-atelier/) (9 months ago)

#### 🐘 My recent toots on mastodon

- [](https://mamot.fr/@arthurlutzim/109482522776151274) (3 days ago)
- [](https://mamot.fr/@arthurlutzim/109482492474198114) (3 days ago)
- [](https://mamot.fr/@arthurlutzim/109462292633140536) (1 week ago)
- [](https://mamot.fr/@arthurlutzim/109461610412247827) (1 week ago)
- [](https://mamot.fr/@arthurlutzim/109461604588673308) (1 week ago)

Want your own self-generating profile page? Check out [readme-scribe](https://github.com/muesli/readme-scribe)!
